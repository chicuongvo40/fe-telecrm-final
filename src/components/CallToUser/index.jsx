// styles
import { useState, useRef, useEffect, useCallback } from 'react';
import styles from './CallToUser.module.css';
import { formartDate, formatNumber } from '~/utils/functions';
import {
  Modal,
  Box,
  Button,
  Typography,
  Paper
} from "@mui/material";
import PhoneIcon from '@mui/icons-material/Phone';
import { DataGrid } from "@mui/x-data-grid";
import VisibilityIcon from '@mui/icons-material/Visibility';
// Modules
import { motion } from 'framer-motion';
import { useAxios } from '~/context/AxiosContex';
import { useQuery, useMutation } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
// context
import { useAuth } from '~/context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { useSip } from '~/context/SipContext';
import { format } from 'date-fns';
export function CallToUser({ setOpenFormCall,idUser,phone,handleOffCallToUser }) {
  const { user } = useAuth();
  const [open, setOpen] = useState(false);
  const [modal, setModal] = useState('');
  const { rejectStt, deviceclv, setRejectStt } = useSip();
  const [idCall, setIdCall] = useState();
  const { getContractByPhone,postCallHistory,getCallHistoryByPhone,getTicketByPhone  } = useAxios();
  const [ticketData, setTicketData] = useState([]);
  const [contractData, setContractData] = useState([]);
  const [callData, setCallData] = useState([]);
  const navigate = useNavigate();

  const [seconds, setSeconds] = useState(0);
  const handleOpen = (op) => {
    setModal(op);
    setOpen(true);
  }
  const handleClose = () => setOpen(false);
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };
  useEffect(() => {
    const timerID = setInterval(() => {
      setSeconds(prevSeconds => prevSeconds + 1);
    }, 1000);

    return () => {
      clearInterval(timerID);
    };
  }, []);
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postCallHistory(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data == '' ) {
        refetch();
        toast('Tạo thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });

      } else {
        toast('Có lỗi trong quá trình tạo CustomerLevel', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });
  // const postCustomerMutation = useMutation({
  //   mutationFn: (data) => {
  //     return postCallHistory(data).then((res) => {
  //       return res;
  //     });
  //   },
  //   onSuccess(data) {
  //     console.log(data + 'Đây là kết quả khi thành công');
  //     if (data == ''  ) {
  //       setIdCall(data.value.id);
  //       refetch();
  //       toast('Tạo thành công', {
  //         icon: '👏',
  //         style: {
  //           borderRadius: '10px',
  //           background: '#333',
  //           color: '#fff',
  //         },
  //       });
  //     } 
  //     // else {
  //     //   toast('Có lỗi trong quá trình tạo lịch sử cuộc gọi', {
  //     //     icon: '👏',
  //     //     style: {
  //     //       borderRadius: '10px',
  //     //       background: '#333',
  //     //       color: '#fff',
  //     //     },
  //     //   });
  //     // }
  //   },
  // });
  const handleAddBrand = (e,content) => {
    e.preventDefault();
    const data = {
      callNumber: phone,
      userId: Object.values(user)[0],
      customerId: idUser,
      realTimeCall: seconds,
      statusCall: content,
      dateCall: format(Date.now(), 'yyyy-MM-dd'),
      recordLink: "string",
      direction: "Inbound",
      totalTimeCall: seconds
    };
    postCustomerMutation.mutate({
      data
    });
  };
  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const remainingSeconds = time % 60;
    const formattedMinutes = minutes < 10 ? `0${minutes}` : minutes;
    const formattedSeconds = remainingSeconds < 10 ? `0${remainingSeconds}` : remainingSeconds;
    return `${formattedMinutes}:${formattedSeconds}`;
  };
  const {
    data: contract,
    isLoading,
    refetch,
  } = useQuery(
    {
      queryKey: ['contract',phone],
      queryFn: () => 
        getContractByPhone(phone)
    }
  );

  useEffect(() => {
    if (contract) {
      setContractData(contract);
    
    }
  }, [contract]);

  const {
    data: ticket,

  } = useQuery(
    {
      queryKey: ['ticket',phone],
      queryFn: () => getTicketByPhone(phone),
    }
  );

  useEffect(() => {
    if (ticket) {
      setTicketData(ticket);
    
    }
  }, [ticket]);

  const {
    data: callhistory,

  } = useQuery(
    {
      queryKey: ['callhistory',phone],
      queryFn: () => getCallHistoryByPhone(phone),
    }
  );

  useEffect(() => {
    if (callhistory) {
      setCallData(callhistory);

    }
  }, [callhistory]);


  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "ticketStatusId",
      headerName: "Status",
      flex: 0.5,
    },
    {
      field: "ticketTypeId",
      headerName: "Type",
      flex: 1,
    },
    {
      field: "levelId",
      headerName: "Level",
      flex: 0.5,
    },
    {
      headerName: "Chi Tiết",
      field: "detail",
      flex: 0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color: 'red', width: '100vw' }}>
          <VisibilityIcon fontSize="small" 
           onClick={() => handleOpen(params.row)} 
          />
        </Box>
      )
    },
  ];

  const columnss = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "dateCall",
      headerName: "DateCall",
      flex: 1,
    },
    {
      field: "direction",
      headerName: "Direction",
      flex: 1,
    },
  ];

  
  const [isSpeakerOn, setIsSpeakerOn] = useState(false);
  const [isMuted, setIsMuted] = useState(false);
  const toggleMute = () => {
    setIsMuted(!isMuted);
  };

  const toggleSpeaker = () => {
    setIsSpeakerOn(!isSpeakerOn);
  };
  return (
    <>
       <div>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box sx={style}>
                <Box display='flex'>
              <Box>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  Thông Tin Ticket   
                  {/* {modal !='' ? modal.id : ''} */}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Tiêu Đề
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Loại Ticket
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Trạng Thái
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Người Thực Hiện
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Level
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Ngày Tạo
                </Typography>
                </Box>
                <Box>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                 ''
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                   {modal !='' ? modal.title : ''} 
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                {modal !='' ? modal.ticketTypeId : ''} 
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                {modal !='' ? modal.ticketStatusId : ''} 
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                {modal !='' ? modal.assignedUserId == null ? 'Chưa có người thực hiện': modal.assignedUserId :''} 
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                {modal !='' ? modal.levelId : ''} 
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                {modal !='' ? formartDate(modal.createdDate , 'full') : ''} 
                </Typography>
                </Box>
                </Box>
                <Box sx={{ mt: 2, display: 'flex' }}>
                  <Button sx={{ color: 'red' }} onClick={handleClose}>Đóng</Button>
                  <Button onClick={handleClose} >Tiếp tục</Button>
                </Box>
              </Box>
            </Modal>
          </div>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ type: 'spring' }}
        className={styles.formmoal}
      >
        <div className={styles.form}>
          <Box m="1.5rem 2.5rem">

            <Box
              display="grid"
              gridTemplateColumns="repeat(2, minmax(0, 1fr))"
              justifyContent="space-between"
              alignItems="center"
              rowGap="20px"
              columnGap="1.33%"
            >

              <Box style={{ width: '40vw', height: '40vh', borderRadius: '10px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                <Paper elevation={3} style={{ padding: '20px', borderRadius: '10px', borderRadius: '8px', backgroundColor: '#f7f7f7', width: '40vw', height: '40vh' }}>
                  {contractData[0] ? (
                    <div>
                      <div style={{ textAlign: 'center', marginBottom: '20px' }}>
                        <Typography variant="h5" gutterBottom style={{ color: '#1a237e' }}>
                          Hợp Đồng
                        </Typography>
                      </div>
                      <div>
                        <Typography gutterBottom style={{ color: '#333' }}>
                          <strong>Mã Hợp Đồng:</strong> <span style={{ color: '#3f51b5' }}>{contractData[0].code}</span>
                        </Typography>
                        <Typography gutterBottom style={{ color: '#333' }}>
                          <strong>Loại Hợp Đồng:</strong> <span style={{ color: '#3f51b5' }}>{contractData[0].contractType}</span>
                        </Typography>
                        <Typography gutterBottom style={{ color: '#333' }}>  
    
                          <strong>Ngày Bắt Đầu:</strong> <span style={{ color: '#3f51b5' }}>{formartDate(contractData[0].timeStart, 'full')}</span>
                        </Typography>
                        <Typography gutterBottom style={{ color: '#333' }}>
                          <strong>Ngày Kết Thúc:</strong> <span style={{ color: '#3f51b5' }}>{formartDate(contractData[0].timeEnd, 'full')}</span>
                        </Typography>
                      </div>
                    </div>
                  ) : (
                    <div style={{ textAlign: 'center' }}>
                      <Typography variant="h5" gutterBottom style={{ color: '#333' }}>
                        Không có dữ liệu Hợp Đồng!
                      </Typography>
                    </div>
                  )}
                </Paper>

              </Box>

              <Paper
                sx={{
                  width: '40vw',
                  height: '40vh',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'rgb(65,65,67)', 
                }}
              >
                <Typography sx={{ fontSize: '40px', textAlign: 'center', mb: 4 }}>
                  {formatTime(seconds)}
                </Typography>
                <Box>
                 <Box>
                  <Button
                    variant={isSpeakerOn ? "outlined" : "contained"}
                    onClick={(e) => handleAddBrand(e, "Bắt Máy")}
      
                    sx={{ ml: 2 }}
                  >
                    {"Answer"}
                  </Button>
                  <Button
                    variant={isSpeakerOn ? "outlined" : "contained"}
                    onClick={(e) => handleAddBrand(e, "Máy bận")}
                  
                
                    sx={{ ml: 2 }}
                  >
                    {"No Answer"}
                  </Button>
                  <Button
                    variant={isSpeakerOn ? "outlined" : "contained"}
                    onClick={(e) => handleAddBrand(e, "Người gọi tắt máy")}
                   
                    sx={{ ml: 2 }}
                  >
                    {"Reject"}
                  </Button>
                  </Box>
                  <Box sx={{
                    mt: 3, // Khoảng cách từ trên xuống
                    display: 'flex', // Sử dụng display: flex để các button trong Box được căn giữa
                    justifyContent: 'center', // Căn giữa các thành phần theo chiều ngang
                    alignItems: 'center', // Căn giữa các thành phần theo chiều dọc
                     }}>
                  <Button
                    variant={isSpeakerOn ? "outlined" : "contained"}
                    sx={{ ml: 2 }}
                    onClick={() =>
                      navigate(`/1/${idUser}/${idCall}/ticket`)
                    }
                  >
                    Tạo Ticket
                  </Button>
                  <Button
                   variant={isMuted ? "outlined" : "contained"}
                    sx={{
                      ml: 2 ,
                      background: 'red',
                    }}
                    onClick={handleOffCallToUser}
                    startIcon={<PhoneIcon />}
                  >
                    {"Kết Thúc"}
                  </Button>
                  </Box>
                </Box>
              </Paper>

              <Box style={{ width: '40vw', height: '40vh' }}>
                <Paper elevation={3} style={{ borderRadius: '10px', borderRadius: '8px', backgroundColor: '#f7f7f7', width: '40vw', height: '45vh' }}>
                  <div style={{ textAlign: 'center' }}>
                    <Typography variant="h5" gutterBottom >
                      Lịch Sử Ticket
                    </Typography>
                  </div>
                  <Box sx={{ height: '35vh', width: '100%' }}>
                    {ticketData ? (
                        <DataGrid
                        getRowId={(row) => row.id + row.ticketStatusId + row.ticketStatusId}
                        rows={ticketData || []}
                        columns={columns}
                        pageSize={5}
                      />
                    ) : (
                      <Typography>Không có dữ liệu</Typography>
                    )
                    }
                  </Box>
                </Paper>
              </Box>

              <Box style={{ width: '40vw', height: '40vh' }}>
                <Paper elevation={3} style={{ borderRadius: '10px', borderRadius: '8px', backgroundColor: '#f7f7f7', width: '40vw', height: '45vh' }}>
                  <div style={{ textAlign: 'center' }}>
                    <Typography variant="h5" gutterBottom >
                      Lịch Sử Cuộc Gọi
                    </Typography>
                  </div>
                  <Box sx={{ height: '35vh', width: '100%' }}>
                  {callData ? (
                       <DataGrid
                        getRowId={(row) => row.id + row.dateCall}
                        rows={callhistory || []}
                        columns={columnss}
                        pageSize={5}
                      />
                    ) : (
                      <Typography>Không có dữ liệu</Typography>
                    )
                    }
                  </Box>
                </Paper>
              </Box>

            </Box>
          </Box>
        </div>
      </motion.div>
    </>
  );
}
