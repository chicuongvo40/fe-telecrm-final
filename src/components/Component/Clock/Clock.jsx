import React, { useState } from 'react'
import './Clock.css'
function Clock() {
  var currentTime = new Date();

  var dateString = currentTime.toLocaleDateString();
  var dateString = dateString.split('/');
  var day = parseInt(dateString[0]);
  var month = parseInt(dateString[1]);
  var year = parseInt(dateString[2]);
  var dateObject = new Date(year, month - 1, day);

  // Mảng các ngày trong tuần
  var daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  // Mảng các tháng trong năm
  var monthsOfYear = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  // Lấy ngày trong tuần, tháng và năm
  var dayOfWeek = daysOfWeek[dateObject.getDay()];
  var monthOfYear = monthsOfYear[dateObject.getMonth()];
  var dayOfMonth = dateObject.getDate();
  var year = dateObject.getFullYear();
  // Chuỗi kết quả
  var resultString = dayOfWeek + ', ' + monthOfYear + ' ' + dayOfMonth + ' ' + year;

  let time = new Date().toLocaleTimeString();
  const [Clock, setClock] = useState(time);
  const [date, setDate] = useState(resultString);

  const UpdateClock = () => {

    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();


    var meridiem = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    if (minutes < 10) {
      minutes = '0' + minutes;
  }

    var timeString = hours + ':' + minutes + ' ' + meridiem;

    setClock(timeString);
  }
  setInterval(UpdateClock, 1000 * 60);
  return (
    <>
      <div className='clock-container'>
        <div>
          <h1 className='clock items-center justify-center'>{Clock}</h1>
          <p className='date items-center justify-center'>{date}</p>
        </div>
      </div>
    </>
  )
}

export default Clock