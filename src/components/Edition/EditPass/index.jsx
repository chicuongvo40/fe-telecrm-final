// Styles
import styles from './EditCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '~/components/Material/Input';
import { Button } from '~/components/Material/Button';
import { useQuery, useMutation } from '@tanstack/react-query';
// Modules
import { motion } from 'framer-motion';
import { useAxios } from '~/context/AxiosContex';
//functions
import { useState } from 'react';
import { useEffect } from 'react';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
export function EditCampaignAdm({ idBrand, setEditModal, refetch}) {
  const { getUserById, updateUser } = useAxios();
  const ods = useRef();
  const [address, setAddress] = useState('');
  const [username, setUsername] = useState('');
  const {
    data: getBrandId,
  } = useQuery(
    {
      queryKey: ['getbrandbyid'],
      queryFn: async () => await getUserById(idBrand),
    }
  );
  useEffect(() => {
    if (getBrandId) {
      setAddress(getBrandId.password);
      setUsername(getBrandId.userName);
    }
  }, [getBrandId]);

  
  const updateBrandMutation = useMutation({
    mutationFn: (data) => {
      return updateUser(data).then((res) => {
       
        return res;
      });
    },
    onSuccess: (data) => { 
      console.log(data);
     
      if (data == '204') {
        refetch();
        toast('Update thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        setEditModal(false);
      }
    },
  });
  function handleUpdateBrand(e) {
    e.preventDefault();
    const dataPatch = {
      id: getBrandId.id,
      branchId: getBrandId.branchId,
      roleId: getBrandId.roleId,
      lastName: getBrandId.lastName,
      firstName: getBrandId.firstName,
      userName: username,
      password: address,
      status: getBrandId.status,
      gender: getBrandId.gender,
      address: getBrandId.address,
      dayOfBirth: getBrandId.dayOfBirth,
      createdDate: getBrandId.createdDate

    };
    if (
    
      !address
    ) {
      toast('Không được bỏ trống mục nào', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    updateBrandMutation.mutate({ data: dataPatch });
  }
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>Thay Đổi Tài Khoản Đăng Nhập</h6>
          <AiOutlineClose
            onClick={() => setEditModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
          <div className={styles.field}>
          <label>Tài Khoản</label>
            <Input
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              type='text'
            />
            <label>Mật Khẩu</label>
            <Input
              value={address}
              onChange={(e) => setAddress(e.target.value)}
              type='text'
            />

          </div>
          
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setEditModal(false);
              }}
            >
              HỦY
            </Button>
            <Button
              onClick={handleUpdateBrand}
              className={styles.addbtn}
            >
              Đổi mật khẩu
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}
