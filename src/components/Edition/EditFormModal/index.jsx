// Styles
import styles from './FormModal.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '~/components/Component/Input';
import { Button } from '~/components/Component/Button';
// Context
import { useAuth } from '~/context/AuthContext';
import { useAxios } from '~/context/AxiosContex';
// Modules
import { motion } from 'framer-motion';
import { useMutation, useQuery } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
import { format, set } from 'date-fns';
import { useState } from 'react';
import { useEffect } from 'react';
import { formartDate, formatNumber } from '~/utils/functions';
export function FormModals({ setOpenModal, refetch,id }) {
  //* Link api
  const { updateCustomer,getSource,getCustomerById,getCustomerLevel} =
    useAxios();
  //* Data từ context
  const { user} = useAuth();

  const [xcustomerLevelId, setcustomerLevelId] = useState('');
  const [xlastName, setlastName] = useState('');
  const [xphoneNumber, setphoneNumber] = useState('');
  const [xfirstName, setfirstName] = useState('');
  const [xaddress, setaddress] = useState('');
  const [xdayOfBirth, setdayOfBirth] = useState('');
  const [xname, setname] = useState('');
  const [xgender, setgender] = useState('');
  const [xsourceId, setsourceId] = useState('');
  const [xemail, setEmail] = useState('');

  const {
    data: source,
  } = useQuery(
    {
      queryKey: ['source'],
      queryFn: async () => await getSource(),
    }
  );

  const {
    data: getBrandId,
  } = useQuery(
    {
      queryKey: ['getbrandbyid'],
      queryFn: async () => await getCustomerById(id),
    }
  );
    const {
    data: level,
  } = useQuery(
    {
      queryKey: ['level'],
      queryFn: async () => await getCustomerLevel(),
    }
  );
  useEffect(() => {
    if (getBrandId) {
      setcustomerLevelId(getBrandId.customerLevelId);
      setlastName(getBrandId.lastName);
      setfirstName(getBrandId.firstName);
      setphoneNumber(getBrandId.phoneNumber);
      setaddress(getBrandId.address);
      setdayOfBirth(format(getBrandId.dayOfBirth, "yyyy-MM-dd"));
      setname(getBrandId.name);
      setgender(getBrandId.gender);
      setsourceId(getBrandId.sourceId);
      setEmail(getBrandId.email);
    }
  }, [getBrandId]);

  const updateBrandMutation = useMutation({
    mutationFn: (data) => {
      console.log(data);
      return updateCustomer(data).then((res) => {
        return res;
      });
    },
    onSuccess: (data) => { 
      
      if (data == '204') {
        toast('Update thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        refetch();
        setOpenModal(false);
        
      }
    },
  });
  function handleUpdateBrand(e) {
    e.preventDefault();
    const dataPatch = {
      id:getBrandId.id,
      customerLevelId: xcustomerLevelId,
      sourceId: xsourceId,
      branchId: getBrandId.branchId,
      lastName: xlastName,
      firstName: xfirstName,
      name: xname,
      phoneNumber: xphoneNumber,
      status: 1,
      gender: xgender,
      address: xaddress,
      dayOfBirth: xdayOfBirth,
      dateCreated:  getBrandId.dateCreated,
      lastEditedTime: format(Date.now(), 'yyyy-MM-dd'),
      email: xemail
    };
    if (
       !xname
    ) {
      toast('Không được bỏ trống mục nào', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    updateBrandMutation.mutate({ data: dataPatch });
  }


  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>Sửa Thông Tin</h6>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
          <div className={styles.field}>
            <label>Họ</label>
            <Input
              value={xfirstName}
              onChange={(e) => setfirstName(e.target.value)}
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Tên</label>
            <Input
             value={xlastName}
             onChange={(e) => setlastName(e.target.value)}
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Họ và Tên</label>
            <Input
            value={xname}
            onChange={(e) => setname(e.target.value)}
              type='text'
            />
          </div>
         
          <div className={styles.field}>
            <label>Số Điện Thoại</label>
            <Input
              onChange={(e) => setphoneNumber(e.target.value)}
              value={xphoneNumber}
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Địa Chỉ</label>
            <Input
            value={xaddress}
            onChange={(e) => setaddress(e.target.value)}
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Email</label>
            <Input
              value={xemail}
              onChange={(e) => setEmail(e.target.value)}
              type='text'
            />
          </div>
          <div className={styles.flex}>
            <div className={styles.field}>
              <label>Giới tính</label>
              <select onChange={(e) => setgender(e.target.value)}    value={xgender}>
                <option value='1'>Nam</option>
                <option value='0'>Nữ</option>
              </select>
            </div>
            <div className={styles.flex}>

            <div className={styles.field}>
            <label>Kênh Khách Hàng</label>
            <select onChange={(e) => setsourceId(e.target.value)} value={xsourceId}>
              {source &&
                source.map((lv, index) => {
                  if (lv?.sourceName) {
                    return (
                      <option
                        key={index}
                        value={lv.id}
                      >
                        {lv.sourceName}
                      </option>
                    );
                  }
                })}
            </select>
          </div>

          
          
            <div className={styles.field}>
              <label>Ngày Sinh</label>
              <Input
                value={xdayOfBirth}
                onChange={(e) => setdayOfBirth(e.target.value)}
                type='date'
              />
            </div>
          </div>
          <div className={styles.field}>
            <label>Cấp Độ</label>
            <select onChange={(e) => setcustomerLevelId(e.target.value)} value={xcustomerLevelId} > 
            {level &&
                level.map((lv, index) => {
                  if (lv?.levelName) {
                    return (
                      <option
                        key={index}
                        value={lv.id}
                      >
                        {lv.levelName}
                      </option>
                    );
                  }
                })}
            </select>
          </div>
         
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setOpenModal(false);
              }}
            >
              Hủy
            </Button>
            <Button
               onClick={handleUpdateBrand}
              className={styles.addbtn}
            >
              Sửa Thông Tin
            </Button>
          </div>
        </div>
        </div>
      </form>
    </motion.div>
  );
}
