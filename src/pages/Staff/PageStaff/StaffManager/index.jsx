import React from 'react'
import { useQuery } from '@tanstack/react-query';
import { useState, useRef, useEffect, useCallback } from 'react';
import AddIcon from '@mui/icons-material/Add';
import { AiFillFileExcel } from 'react-icons/ai';
import { Button, Typography, Box } from '@mui/material';
import DataGridCustomToolbar from "~/components/Material/DataGridCustomToolbar/DataGridCustomToolbar";
import { FcCallback } from 'react-icons/fc';
import { CgDetailsMore } from 'react-icons/cg';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { TiDelete } from 'react-icons/ti';
import EditIcon from '@mui/icons-material/Edit';
import { DataGrid } from "@mui/x-data-grid";
//Component
import { CallToUser } from '~/components/CallToUser';
import { FormModal } from '~/components/Addition/FormModal';
import { ExcelData } from '~/components/ExcelData';
import { FormModals } from '~/components/Edition/EditFormModal';
import { DetailCustomer } from '~/components/Edition/DetailCustomer';
//Context
import { useSip } from '~/context/SipContext';
import { useAxios } from '~/context/AxiosContex';
//hook
import { useExcel } from '~/hooks/useExcel';
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { useAuth } from '~/context/AuthContext';
export default function StaffManager() {
  const { user } = useAuth();
  //Api link
  const { getCustomer } = useAxios();
  //Usestate
  const [customerData, setCustomerData] = useState([]);
  const [idUser, setIdUser] = useState(null);
  const [phone, setPhone] = useState(null);
  const [openFormCall, setOpenFormCall] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [search, setSearch] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const [id, setId] = useState("");
  const [openEditModal, setEditOpenModal] = useState(false);
  const [openDetailCustomer, setDetailCustomer] = useState(false);
  function refresh() {
    refetch()
  }
  function handleOnClick(id) {
    setEditOpenModal(true);
    setId(id);
  }
  function handleOnClick1(id) {
    setDetailCustomer(true);
    setId(id);
  }

  const getStatusColor = (status) => {
    return status === '1' ? 'green' : 'red';
  };




  //UsePhone
  const { deviceclv } = useSip();

  // Call to users
  function handleCallToUser(
    number) {
    deviceclv?.current.initiateCall(number);
    setOpenFormCall(true);
  }
// 
  function handleOffCallToUser(
    number) {
            deviceclv?.current.reject();

    setOpenFormCall(false);
  }
  // get customers
  const {
    data: customers,
    isLoading,
    refetch,
  } =
    useQuery(
      {
        queryKey: ['customers'],
        queryFn: async () => await getCustomer(Object.values(user)[4]),
      }
    );

  useEffect(() => {
    if (customers) {
      setCustomerData(customers);

    }
  }, [customers]);
  const [
    handleFile,
    excelFileError,
    excelData,
    setExcelFile,
    setOpenExcel,
    openExcel,
    setExcelData,
  ] = useExcel();
  if (excelFileError) {
    toast(excelFileError, {
      icon: '👏',
      style: {
        borderRadius: '10px',
        background: '#333',
        color: '#fff',
      },
    });
  }

  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {params.row.id}
        </Typography>
      )
    },
    {
      field: "name",
      headerName: "TÊN",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {params.row.name}
        </Typography>
      )
    },
    {
      field: "gender",
      headerName: "GIỚI TÍNH",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {params.row.gender === '1' ? 'Nam' : 'Nữ'}
        </Typography>
      )
    },
    {
      field: "address",
      headerName: "ĐỊA CHỈ",
      flex: 0.2,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {params.row.address}
        </Typography>
      )
    },
    {
      field: "status",
      headerName: "TRẠNG THÁI",
      renderCell: (params) => (
        <Typography color={getStatusColor(params.row.status)} variant="body1" fontSize="13px">
          {params.row.status === '1' ? 'Hoạt Động' : 'Hết Hạn'}
        </Typography>
      )
    },
    {
      field: "phoneNumber",
      headerName: "ĐIỆN THOẠI",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {params.row.phoneNumber}
        </Typography>
      )
    },
    {
      headerName: "CALL",
      field: "call",
      flex: 0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100vw' }}>
          <FcCallback
            onClick={
              () => {
                handleCallToUser(params.row.phoneNumber)
                setIdUser(params.row.id)
                setPhone(params.row.phoneNumber)
              }
            }
          />
        </Box>
      )
    },
    {
      headerName: "DETAIL",
      field: "detail",
      flex: 0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color: 'red', width: '100vw' }}>
          <VisibilityIcon fontSize="small" onClick={() => handleOnClick1(params.row.id)} />
        </Box>
      )
    },
    {
      headerName: "EDIT",
      field: "edit",
      flex: 0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100vw' }}>
          <EditIcon fontSize="small" onClick={() => handleOnClick(params.row.id)} />
        </Box>
      )
    }

  ];
  return (
    <div className='staffcontent min-w-max max-w-screen-2xl'>
      {openExcel ? (
        <ExcelData
          refetch={refetch}
          setExcelData={setExcelData}
          setOpenExcel={setOpenExcel}
          excelData={excelData}
          setExcelFile={setExcelFile}
        />
      ) : null}

      {openModal ? (
        <FormModal
          refetch={refetch}
          setOpenModal={setOpenModal}
        />
      ) : null}

      {openFormCall ? (
        <CallToUser
          handleOffCallToUser={handleOffCallToUser}
          setOpenFormCall={setOpenFormCall}
          idUser={idUser}
          phone={phone}
        />
      ) : null}

      {openEditModal ? (
        <FormModals
          id={id}
          refetch={refresh}
          setOpenModal={setEditOpenModal}
        />
      ) : null}

      
{openDetailCustomer ? (
        <DetailCustomer
          id={id}
          setOpenModal={setDetailCustomer}
        />
      ) : null}

      <div >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <p className='font-bold' style={{ fontFamily: 'Roboto, sans-serif', fontSize: '20px', color: '#1E0342' }}>
           <HomeIcon/>  <ArrowForwardIosIcon/> Khách Hàng
          </p>
          <Button style={{ backgroundColor: '#2196f3', color: '#fff', marginLeft: 'auto', marginBottom: 0 }} startIcon={<AddIcon />} onClick={() => setOpenModal(true)}>
            <Typography style={{ fontSize: 12 }} >Khách Hàng</Typography>
          </Button>

          <Button style={{ backgroundColor: '#2196f3', color: '#fff', marginLeft: '10px' }} startIcon={<AiFillFileExcel />} >
            <Typography id='ulbtn'
              component="label"
              htmlFor='upload-photo' style={{ fontSize: 12, color: 'white' }} >Thêm excelss</Typography>
          </Button>

        </div>
        <div >
          <div>
            <input
              className='choosefile'
              onChange={handleFile}
              type='file'
              name='photo'
              id='upload-photo'
            />
          </div>
        </div>
        {customerData ? (<div className=''>
          <Box>
            <Box
              mt="0px"
              height="75vh"
              width='77.9vw'
              sx={{
                "& .MuiDataGrid-root": {
                  border: "none",
                },
                "& .MuiDataGrid-cell": {
                  borderBottom: "none",
                },
                "& .MuiDataGrid-columnHeaders": {
                  backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
                  color: "black", // Màu trắng
                  borderBottom: "none",
                },
                "& .MuiDataGrid-virtualScroller": {
                  backgroundColor: "#FFF", // Màu xanh dương nhạt
                },
                "& .MuiDataGrid-footerContainer": {
                  backgroundColor: "#black", // Màu xanh dương
                  color: "#FFF", // Màu trắng
                  borderTop: "none",
                },
                "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                  color: "#2196F3 !important", // Màu đỏ
                },
                ".MuiDataGrid-toolbarContainer .MuiText": {
                  color: "#2196F3 !important", /* Màu văn bản */
                },
                '& .MuiDataGrid-cell': {
                  fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
                },
                '& .MuiDataGrid-columnHeader': {
                  fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
                },
              }}
            >
              <DataGrid
                loading={isLoading || !customerData}
                rows={customerData || []}
                columns={columns}
                getRowId={(row) => row.id + "acxas"}
                components={{ Toolbar: DataGridCustomToolbar }}
                componentsProps={{
                  toolbar: { searchInput, setSearchInput, setSearch },
                }}
              />
            </Box>
          </Box>
          {/* <UserData
            refetch={refetch}
            handleCallToUser={handleCallToUser}
            isLoading={isLoading}
            groupNamed={customerData}
            setIdUser={setIdUser}
            setPhone={setPhone}
            handleOffCallToUser={handleOffCallToUser}
          /> */}
        </div>) : null}

      </div>
    </div>
  );
}
