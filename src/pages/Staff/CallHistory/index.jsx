import React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import {
  Box,
  Typography,
  styled,
  Table,
  Chip
} from "@mui/material";
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import { useQuery } from '@tanstack/react-query'; // Removed useMutation as it's not used
import img from '~/assets/frequency.png'
import { useAxios } from '~/context/AxiosContex';
import { useState, useEffect } from 'react';
import { formartDate, formatNumber } from '~/utils/functions';
import { DataGrid } from "@mui/x-data-grid";
import { Loading } from '~/components/LoadingPage/Loading';
import DataGridCustomToolbar from "~/components/Material/DataGridCustomToolbar/DataGridCustomToolbar";
import { useAuth } from '~/context/AuthContext';
export default function HistoryCall() {
  const { user } = useAuth();
  const [callData, setCallData] = useState([]);
  const { getCallHistory } = useAxios();
  const {
    data: callhistory,
    isLoading
  } = useQuery(
    {
      queryKey: ['callhistory'],
      queryFn: () => getCallHistory(Object.values(user)[0]),
    }
  );

  useEffect(() => {
    if (callhistory) {
      setCallData(callhistory);
      console.log(callhistory);
    }
  }, [callhistory]);

  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.2,
    },
    {
      field: "callNumber",
      headerName: "Số Điện Thoại",
      flex: 0.7,
    },
    {
      field: "dateCall",
      headerName: "Ngày Gọi",
      flex: 1.5,
      renderCell: (params) => (
        <Typography>{formartDate(params.row.dateCall, 'full')}</Typography>
      )
    },
    {
      field: "direction",
      headerName: "Hướng Gọi",
      flex: 1,
    },
    {
      field: "statusCall",
      headerName: "Trạng Thái",
      headerAlign: 'center',
      flex: 1,
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color: 'blue', width: '100vw' }}>
          <Chip
            label={params.row.statusCall}
            color={params.row.statusCall === 'Bắt máy' ? "primary" :
              params.row.statusCall === 'Máy bận' ? "secondary" :
                params.row.statusCall === 3 ? "Người gọi tắt máy" : "default"}
          />
        </Box>

      )
    },
    {
      headerName: "Record",
      field: "Record",
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color: 'blue', width: '100vw' }}>
          <PlayCircleIcon fontSize="small" />
          <img src={img} style={{height: '8px',marginLeft:'1px'}} alt="Description" />
        </Box>

      )
    },
    {
      field: "realTimeCall",
      headerName: "Thời Gian",
      flex: 1,
    },

  ];
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  return (
    <>
      
        <Container>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Typography style={{ fontFamily: 'Arial, sans-serif', fontSize: '20px', fontWeight: 'bold', color: '#1E0342' }}>
            <HomeIcon/>  <ArrowForwardIosIcon/> Lịch Sử Cuộc Gọi
            </Typography>


          </div>
          <Box
            width="77.9vw"
          >

            <Box style={{ width: '100%' }}
              sx={{
                "& .MuiDataGrid-root": {
                  border: "none",
                },
                "& .MuiDataGrid-cell": {
                  borderBottom: "none",
                },
                "& .MuiDataGrid-columnHeaders": {
                  backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
                  color: "black", // Màu trắng
                  borderBottom: "none",
                },
                "& .MuiDataGrid-virtualScroller": {
                  backgroundColor: "#FFF", // Màu xanh dương nhạt
                },
                "& .MuiDataGrid-footerContainer": {
                  backgroundColor: "#black", // Màu xanh dương
                  color: "#FFF", // Màu trắng
                  borderTop: "none",
                },
                "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                  color: "#2196F3 !important", // Màu đỏ
                },
                ".MuiDataGrid-toolbarContainer .MuiText": {
                  color: "#2196F3 !important", /* Màu văn bản */
                },
                '& .MuiDataGrid-cell': {
                  fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
                },
                '& .MuiDataGrid-columnHeader': {
                  fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
                },
              }}
            >
              <DataGrid
                rows={callData || []}
                getRowId={(row) => row.id}
                columns={columns}
                pageSize={5}
                autoHeight
                components={{ Toolbar: DataGridCustomToolbar }}
                loading={isLoading}
              />
            </Box>

          </Box>
        </Container>
     
    </>
  );
}
