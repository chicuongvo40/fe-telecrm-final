import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';
import { router } from './router';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import './index.css'
import { ThemeProvider } from "@material-tailwind/react";
const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById('root')).render(
  <QueryClientProvider client={queryClient}>
    <ThemeProvider>
    <RouterProvider router={router} />
    </ThemeProvider>
  </QueryClientProvider>,
);



// import React from 'react'
// import ReactDOM from 'react-dom/client'
// import App from './App.jsx'
//import { ThemeProvider } from "@material-tailwind/react";
// ReactDOM.createRoot(document.getElementById('root')).render(
//   <React.StrictMode>
//     <ThemeProvider>
//     <App />
//     </ThemeProvider>
//   </React.StrictMode>,
// )
